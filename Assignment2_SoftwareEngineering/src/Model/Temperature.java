/**
 * Created by Devon on 11/3/2015.
 */

package Model;
import java.util.Observable;

    public class Temperature extends Observable {

    public static String cUnit = " °C";
    private double tempF;
    private double tempC;

    public Temperature() {
        tempF =0;

    }

    public Temperature(double tempF) {
        this.setTempF(tempF);
        setChanged();
        notifyObservers();
    }

    public double getTempF() {
        return tempF;


    }

    public void setTempF(double tempF) {
        this.tempF = tempF;
        setChanged();
        notifyObservers();
    }

    public double getTempC() {
        convertFtoC();
        return tempC;
    }

    public void convertFtoC()
    {
        tempC = (getTempF()-32) * 5/9;

    }


    public void setTempC(double tempC)
    {
        this.tempC = tempC;
    }
    @Override
    public String toString()
    {
        String output = Double.toString(getTempC()) + cUnit;

        return output;

    }

}

package View;

import Model.Temperature;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

/**
 * Created by Devon on 11/6/2015.
 */
public class Tfview extends JTextField implements Observer {
   // Logger logger = Factory.getLogger(Tfview.class);
    private Temperature temperature;
    public Tfview(Temperature _temperature) {
        this.temperature = _temperature;
    }

    @Override
    public void update(Observable o, Object arg) {
        //Do something similar with JslideView.
        //setText() method is used for change text in the field.
        //setText("32");
        this.setText(temperature.toString());

    }
}

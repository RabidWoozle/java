package View;

import Model.Temperature;

import javax.swing.*;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Devon on 11/6/2015.
 */
public class JslideView  extends JSlider implements Observer { // This is an observer.
    Temperature temperature;

    //Hashtable<Integer, String> labels = new Hashtable<Integer, String>();



    public JslideView(Temperature _temperature) {
        super();

        setMaximum(100);
        setMinimum(-100);
        setValue(0);
        setMajorTickSpacing(100);
        setMinorTickSpacing(50);
        setPaintTicks(true);
        setPaintLabels(true);
        //Create the label table for the Jslider
        this.temperature = _temperature;

    }

    public void update(Observable o, Object arg) {
        //update() is executed when Observable is changed.

        // Observable o is a model
        //You can change the position of the knob by calling setValue().
        if(o == temperature) {
         //   temperature.convertFtoC();
            setValue((int) temperature.getTempC());
        }
    }
}

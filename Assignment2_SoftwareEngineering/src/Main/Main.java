/**
 * Created by Devon on 11/3/2015.
 */
package Main;

import Model.Temperature;

import View.*;
import Controller.TemperatureController;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {

        Temperature temperature = new Temperature();
        JslideView jslide = new JslideView(temperature);
        Tfview txview =  new Tfview (temperature);
        //ConsoleView console = new ConsoleView(temperature);
        JTextField textField = new JTextField();
        temperature.addObserver(jslide);
        temperature.addObserver(txview);
        textField.addActionListener(new TemperatureController(temperature));

        JFrame frame = new JFrame("F to C Converter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(textField, BorderLayout.NORTH);
        frame.add(jslide, BorderLayout.CENTER);
        frame.add(txview, BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

}

package Controller;

/**
 * Created by Devon on 11/6/2015.
 */

import Model.Temperature;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import View.JslideView;

public class TemperatureController implements ActionListener {
    private Temperature temperature;

    public TemperatureController(Temperature _temperature) {
        this.temperature = _temperature;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Invoked when an action occurs (e.g., key press).
       //temperature.setTempF();
       String cmd = e.getActionCommand();
        if(isNumber(cmd))
        {
            temperature.setTempF(Double.parseDouble(cmd));

        }

    }

    public boolean isNumber(String s)
    {
        try{
            double n = Double.parseDouble(s);

        }
        catch(NumberFormatException nfe)
        {
            return false;

        }

        return true;

    }
}

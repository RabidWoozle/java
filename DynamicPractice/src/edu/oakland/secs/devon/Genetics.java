package edu.oakland.secs.devon;

import com.sun.xml.internal.fastinfoset.util.CharArray;
import java.util.Arrays;
import java.lang.Character;
/**
 * Created by Devon on 7/18/2016.
 */
public class Genetics {

    public static void main(String args[]){

        System.out.println(getOffspring("AbcDefED","AbcdefeD", "DRDRDDRD"));
    }

    public static String getOffspring(String g1, String g2, String dom)
    {


        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < g1.length(); i++) {

            if( dom.charAt(i) == 'D' )
            {
                if(Character.isUpperCase(g1.charAt(i)))
                {
                    sb.append(g1.charAt(i));
                }
                else if(Character.isUpperCase(g2.charAt(i)))
                {
                    sb.append(g2.charAt(i));
                }
                else
                {
                    sb.append(g1.charAt(i));
                }
            }
            else{

                if(Character.isLowerCase(g1.charAt(i)))
                {
                    sb.append(g1.charAt(i));
                }
                else if(Character.isLowerCase(g2.charAt(i)))
                {
                    sb.append(g2.charAt(i));
                }
                else
                {
                    sb.append(g1.charAt(i));
                }

            }

        }

        return sb.toString();

    }

}

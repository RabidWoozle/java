package edu.oakland.secs.devon;

/**
 * Created by Devon on 6/27/2016.
 */
public class threeandfive {

    public static void main(String [] args){

        long lastNumber = 0;

        for(int i = 0; i < 1000; i++)
        {
            if(i%3 == 0 || i%5 == 0 )
            lastNumber += i;

        }

        System.out.println(lastNumber);
    }
}

package edu.oakland.secs.devon;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        File in = new File("C:\\Users\\Devon\\IdeaProjects\\DynamicPractice\\src\\edu\\oakland\\secs\\devon\\A-small-practice-1.in");

        File in2 = new File("C:\\Users\\Devon\\IdeaProjects\\DynamicPractice\\src\\edu\\oakland\\secs\\devon\\A-small-practice-2.in");

        sortTroublesome(in);

        sortTroublesome(in2);

    }


    public static int smallestNumberOfCoins(int[] coins, int sum, int index, int count)
    {
        if(sum == 0 || index == coins.length)
        {
            return 0;

        }

        if (sum < 0)
        {
            return 0;

        }

        int countUsingIndex = smallestNumberOfCoins(coins, sum - coins[index], index, count +1);
        int countWithoutIndex = smallestNumberOfCoins(coins, sum, index +1, count);
        if(countUsingIndex == 0)
        {
            return countWithoutIndex;
        }
        if(countWithoutIndex == 0){

            return countUsingIndex;
        }

        return Math.min(countUsingIndex, countWithoutIndex);




    }

    public static void sortTroublesome(File input){

        Scanner scn = null;

        try{
            scn = new Scanner(input);

        }
        catch(Exception e){
            System.out.println(e.toString());

        }

        int numberOfTests = scn.nextInt();
        for(int i = 0; i < numberOfTests; i++)
        {


            if(scn.hasNextLine())
            {
                int testCases = scn.nextInt();
                scn.nextLine();
                List<String> leftList = new ArrayList<String>();
                List<String> rightList = new ArrayList<String>();

                if(testCases == 1)
                {
                    System.out.println("Case " + (i+1) +": Yes" );
                    scn.nextLine();
                }
                else
                for(int n = 0; n < testCases; n++)
                {

                    String workingStr = scn.nextLine();
                    String[] pair = workingStr.split(" ");
                    if(!leftList.contains(pair[1]) && !rightList.contains(pair[0]))
                    {
                        if(!leftList.contains(pair[0]))
                        leftList.add(pair[0]);

                        if(!rightList.contains(pair[1]))
                        rightList.add(pair[1]);

                        if(n == testCases - 1)
                        {
                            System.out.println("Case " + (i+1) + ": Yes");
                            while(!scn.hasNextInt() && scn.hasNext())
                            {
                                scn.nextLine();

                            }
                            break;


                        }

                    }
                    else
                    {
                        System.out.println("Case " + (i+1) + ": No");
                        while(!scn.hasNextInt() && scn.hasNext())
                        {
                           scn.nextLine();

                        }
                        break;
                    }




                }
            }

        }





    }

    public static void sumOfMultiples(int belowMax)
    {
               // belowMax
    }
}

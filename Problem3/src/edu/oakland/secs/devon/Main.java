package edu.oakland.secs.devon;

public class Main {

    public static void main(String[] args) {
	// write your code here
    //Adapter inherits TravelAgency
        //Adapter uses object of LegacyCode and calls that method and adapts it.
        //concrete target also inherits the abstract TravelAgency class

        //create reservation in Travel agency is redefined in Class
        //old cancel and old make
        Adapter adapter = new Adapter();
        LegacyReservations lr = new LegacyReservations();


        adapter.makeReservations();
        adapter.cancelReservations();

        lr.makeReservation();
        lr.cancelReservations();

    }
}

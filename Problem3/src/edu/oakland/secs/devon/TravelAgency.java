package edu.oakland.secs.devon;

/**
 * Created by Devon on 11/19/2015.
 */
 abstract public class TravelAgency {

    abstract void makeReservations();

    abstract void cancelReservations();

        }

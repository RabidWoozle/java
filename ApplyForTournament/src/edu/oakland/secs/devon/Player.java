package edu.oakland.secs.devon;

/**
 * Created by Devon on 11/7/2015.
 */
public class Player {
    private int expertiseRating =0;
    private String name;

    Player(String name){
        this.name = name;

    }

    Player(String name, int expertiseRating){

        this.name = name;

        this.expertiseRating = expertiseRating;

    }


    public int getExpertiseRating() {
        return expertiseRating;
    }

    public void notify(boolean bool, String message)
    {
        if(bool)
        System.out.println(":Accepted Reason:"+message);
        else
        {
            System.out.println(":Rejected Reason:"+message);
        }

    }


}

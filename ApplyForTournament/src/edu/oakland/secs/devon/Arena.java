package edu.oakland.secs.devon;

/**
 * Created by Devon on 11/7/2015.
 */
public class Arena {

    private int maxOfTournament;

    private League league;

    Arena(){

        maxOfTournament = 0;


    }

    Arena(int tMax)
    {

       setMaxOfTournament(tMax);

    }



    public void setMaxOfTournament(int maxOfTournament) {
        this.maxOfTournament = maxOfTournament;
    }

    public void applyForTournament(Tournament tournament, Player player, int rating){

        //TODO
        league = new League(tournament, rating);

        if(tournament.isReachedMaxPlayer())
        {
            player.notify(false, "Tournament is full");
        }
        else
        {
            if(league.isRegistered(player))
            {
                player.notify(true, "You are registered and have been admitted to the tournament");

            }
            else if(league.isInRange(player.getExpertiseRating()))
            {

               if(league.addPlayer(player))
               {
                   player.notify(true, "You are accepted");

               }

            }
            else {

                player.notify(false, "You are required to reach minimum expertise rating");

            }
            }
        }
    }


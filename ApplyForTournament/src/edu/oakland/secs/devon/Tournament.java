package edu.oakland.secs.devon;

import java.util.List;

/**
 * Created by Devon on 11/7/2015.
 */
public class Tournament {
    private int currentPlayer;
    private int maxPlayer;
    private String name;
    private Match scheduledMatches;
    //private List<Player> players;

//    private League league = new League();
    //    private Match scheduledMatches;


    public Tournament(int currentPlayer, int maxPlayer) {
        this.currentPlayer = currentPlayer;
        this.maxPlayer = maxPlayer;
    }

    public boolean checkIsFull()
{
    if(currentPlayer == maxPlayer)
        return true;
    else
        return false;
}

    public boolean isReachedMaxPlayer()
    {
        return checkIsFull();

    }



}

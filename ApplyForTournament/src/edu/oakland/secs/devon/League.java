package edu.oakland.secs.devon;

import java.util.*;

/**
 * Created by Devon on 11/7/2015.
 */
public class League {
    private int minExpertiseRating;
    public List<Player> registeredPlayers = new ArrayList<Player>();
    private Tournament tournament;


//    private Arena arena = new Arena();

    public League(Tournament tournament, int minExpertiseRating)
    {
        this.tournament = tournament;
        this.minExpertiseRating = minExpertiseRating;


    }
    public boolean isRegistered(Player player)
    {
        return checkRegistered(player);

    }
    public boolean checkRegistered(Player player)
    {
        if(registeredPlayers == null)
        {
            return false;
        }
        else if(registeredPlayers.contains(player))
        {
            return true;
        }
        else
            return false;

    }


    public boolean isInRange(int expertiseRating)
    {

        return checkRange(expertiseRating);
    }

    private boolean checkRange(int rating) {

        if(rating >= minExpertiseRating)
            return true;
        else
            return false;
    }

    public boolean addPlayer(Player player)
    {

      if(registeredPlayers.add(player))
      {
          return true;
      }
        else
          return false;

    }



}

package edu.oakland.secs.devon;

public class Main {

    public static void main(String[] args) {
	// write your code here

    Player player1 = new Player("Test Player", 9);
        Player player2 = new Player("Player 2", 0);

        Tournament tournament, tournament1, tournament2;
        tournament = new Tournament( 3,5);
        tournament1 = new Tournament(5,5);
        tournament2= new Tournament(4,5);

        Arena arena = new Arena();

        arena.applyForTournament(tournament, player1, 8);
        arena.applyForTournament(tournament1,player1, 5);
        arena.applyForTournament(tournament2, player2, 0);
        arena.applyForTournament(tournament, player2, 8);



    }
}

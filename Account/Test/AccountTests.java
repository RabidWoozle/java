/**
 * Created by Devon on 11/18/2015.
 */
import edu.oakland.secs.devon.Account;
import org.junit.Assert;
import org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class AccountTests {

/**   Account1: "Ted Murphy", 72354, 102.56
 *  Account2: "Jane Smith", 69713, 40.00
 *  Account3: "Edward Demsey", 93757, 759.32
 *  Account4 is an alias of Account1
**/

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;



    @Before
    public void setUp() throws Exception
    {
        account1 = new Account("Ted Murphy", 72354, 102.56 );
        account2 = new Account("Jane Smith", 69713, 40.00);
        account3 = new Account("Edward Demsey",93757,759.32);
        account4 = account1;
    }
    @Test
    public void testAlias() throws Exception
    {

        Assert.assertEquals(account1, account4);

    }
    @Test
    public void testDeposit () throws Exception
    {
        Assert.assertTrue("This failed", 202.56 == account1.deposit(100));

    }

    @Test(expected = ArithmeticException.class)
    public void testDepositException() throws Exception
    {
        account1.deposit(-20);

    }

    @Test
    public void testWithdraw() throws Exception
    {
        Assert.assertTrue("Failed balance not correct", 28 == account2.withdraw(10, 2));


    }

    @Test
    public void testAddInterest()
    {
        Assert.assertTrue("Failed interest calculated balance is wrong", 785.8962 == account3.addInterest());

    }

    @Test
    public void testToString()
    {
        Assert.assertTrue("This failed the toString returned bad value",account2.toString().equalsIgnoreCase("69713\tJane Smith\t$40.00"));

    }
    @After
    public void tearDown() throws Exception
    {
        account1 = null;
        account2 = null;
        account3 = null;
        account4 = null;

        Assert.assertNull(account1);
        Assert.assertNull(account2);
        Assert.assertNull(account3);
        Assert.assertNull(account4);





    }

}

package edu.oakland.secs.devon;

import java.text.NumberFormat;

import static java.text.NumberFormat.getCurrencyInstance;

/**
 * Created by Devon on 11/8/2015.
 */
public class Account {

    private final double RATE = .035;

    private long acctNumber;
    private double balance;
    private String name;

    public Account (String owner, long account, double initial)
    {
        name = owner;
        acctNumber = account;
        balance = initial;



    }

   public double deposit(double amount)
   {
       if(amount <=0) throw new ArithmeticException("Not allowed negative value for deposit");
       balance = balance +amount;

       return balance;


   }

    public double withdraw(double amount, double fee)
    {
        balance = balance - amount -fee;

        return balance;

    }

    public double addInterest()
    {
        balance+=(balance*RATE);

        return balance;


    }

    public double getBalance()
    {
        return balance;

    }

    public String toString()
    {

        NumberFormat fmt = getCurrencyInstance();

        return(acctNumber+ "\t"+ name + "\t" + fmt.format(balance));
    }
}
